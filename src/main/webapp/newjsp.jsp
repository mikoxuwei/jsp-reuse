<%-- 
    Document   : newjsp
    Created on : 2022年11月1日, 下午2:26:06
    Author     : miko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form method="post" action="redirectServlet">
            URL: <input name="url"/><input type="submit"/> 
            <!--貼入網址，能直接轉到網址去，通過redirectServlet-->
        </form>
        
        <%
//            out.println("123");
//            out.flush();
//            只要要轉址，最好不要有任何輸出，
//            response.sendRedirect("index.jsp");
//            response.sendRedirect("https:"//www.yzu.edu.tw/");
//            只要是一個存在的網址皆可，因為是重新做一次連結 
        %>
        <%--<jsp:forward page = ""/> 只限專案內的頁面--%>
    </body>
</html>
