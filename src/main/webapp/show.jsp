<%-- 
    Document   : show
    Created on : 2022年10月25日, 下午3:35:09
    Author     : miko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="/WEB-INF/date.jsp" %>
        <%
            request.setCharacterEncoding("utf-8"); // 可以顯示中文
            
            String date = request.getParameter("date");
            String content = request.getParameter("content");
            
            session.setAttribute("date", date);
            session.setAttribute("content", content);
            
            content = content.replace("<", "&lt"); // 修改不會動到原始字串
            content = content.replace(">", "&gt"); // 這兩行替換掉大於小於，可以避免人家在網頁上輸入指令，但是還是一樣會顯示大於小於
            
            out.println(date);
            out.println("<br/>");
            out.println(content);
        %>
    </body>
</html>
